import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
        
    @IBOutlet var imageView: UIImageView!
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
    }
}
