import Foundation
import UIKit
import PKHUD

class CollageViewController: UIViewController {
    
    var imageArray : [UIImage] = []
    var imageViewArray: [CollageImageView] = []
    var index: Int = 0
    
    var imagesAPI: ImagesAPI = ImagesAPI.shared
    
    @IBOutlet weak var collageView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for image in imagesAPI.getImages() {
            imageArray.append(UIImage(cgImage: image))
        }
        updateUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        imageArray = []
        imageViewArray = []
        for image in imagesAPI.getImages() {
            imageArray.append(UIImage(cgImage: image))
        }
        updateUI()
    }
    
    func updateUI() {
        
        title = "your Collage"
        let letBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(back))
        let rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveCollage))
        navigationItem.rightBarButtonItem = rightBarButtonItem
        navigationItem.leftBarButtonItem = letBarButtonItem
        if let collage = CollageView.loadFromNibNamed(nibNamed: "CollageView", bundle: nil) as? CollageView {
            collage.frame = CGRect(x: 0.0, y: 0.0, width: collageView.frame.size.width, height: collageView.frame.size.height)
            collageView.addSubview(collage)
            self.insertImage(view: collage)
        }
    }
    
    @objc func saveCollage() {
        let image = collageView.getSnapshotImage()
//        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        imagesAPI.setCollage(image: image)
        HUD.flash((.label("Image saved to Camera Roll")), delay: 0.5)
    }
    
    @objc func back() {
        imagesAPI.removeImages()
        _ = navigationController?.popViewController(animated: true)
    }
    
    
    func insertImage(view: CollageView) {
        getSubviewsOf(view: view)
        
        if imageArray.count == imageViewArray.count {
            for (index, image) in imageArray.enumerated(){
                let imageViewSize = imageViewArray[index].frame.size
                if imageViewSize.width > image.size.width{
                    imageViewArray[index].contentMode = .scaleAspectFit
                }else{
                    imageViewArray[index].contentMode = .scaleAspectFill
                }
                imageViewArray[index].image = image
            }
        }
    }
    
    func getSubviewsOf(view: UIView) {
        for subview in view.subviews {
            if subview is CollageImageView{
                let imageView = subview as! CollageImageView
                imageView.imageIndex = index
                imageViewArray.append(imageView)
                index = index < 4 ? index + 1 : 0
            }else {
                getSubviewsOf(view: subview)
            }
        }
    }
}
