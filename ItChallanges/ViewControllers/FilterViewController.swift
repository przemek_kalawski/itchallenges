import Foundation
import UIKit
import PKHUD

class FilterViewController: UIViewController {

    var xCoord: CGFloat = 5
    let yCoord: CGFloat = 5
    let buttonWidth: CGFloat = 70
    let buttonHeight: CGFloat = 70
    let gapBetweenButtons: CGFloat = 5
    
    public var imageIndex: Int = 0
    private var filterIndex: Int = 0
    
    @IBOutlet weak var filtersScrollView: UIScrollView!
    @IBOutlet weak var imageToFilter: UIImageView!
    @IBOutlet weak var originalImage: UIImageView!
    @IBOutlet weak var containerView: UIView!
    
    var CIFilterNames = [
        "CIPhotoEffectChrome",
        "CIPhotoEffectFade",
        "CIPhotoEffectInstant",
        "CIPhotoEffectNoir",
        "CIPhotoEffectProcess",
        "CIPhotoEffectTonal",
        "CIPhotoEffectTransfer",
        "CISepiaTone"
    ]

    override func viewDidLoad() {
        super.viewDidLoad()

        originalImage.image = UIImage(cgImage: ImagesAPI.shared.getImage(index: imageIndex))
        addButtons()
        
        let leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(savePictButton))
        navigationItem.leftBarButtonItem = leftBarButtonItem
    }
    
    func addButtons(){
        var imageCount: Int = 0
        
        for i in 0..<CIFilterNames.count  {
            imageCount = i
            let filterButton = UIButton(type: .custom)
            filterButton.frame = CGRect(x: xCoord, y: yCoord, width: buttonWidth, height: buttonHeight)
            filterButton.tag = imageCount
            filterButton.addTarget(self, action: #selector(filterButtonTapped), for: .touchUpInside)
            filterButton.layer.cornerRadius = 6
            filterButton.clipsToBounds = true
            
            let filteredImageRef = setFilters(index: imageCount)
            let imageForButton = UIImage(cgImage: filteredImageRef)
            
            filterButton.setBackgroundImage(imageForButton, for: .normal)
            
            xCoord += buttonWidth + gapBetweenButtons
            filtersScrollView.addSubview(filterButton)
        }
        
        filtersScrollView.contentSize = CGSize(width :buttonWidth * CGFloat(imageCount+2), height: yCoord)
    }
    
    @objc func filterButtonTapped(sender: UIButton) {
        
        let button = sender as UIButton
        filterIndex = button.tag
        imageToFilter.image = button.backgroundImage(for: UIControlState.normal)
    }
    
    @IBAction func savePictButton(_ sender: Any) {
        
        ImagesAPI.shared.replaceImage(image: setFilters(index: filterIndex), index: imageIndex)
        HUD.flash((.label("Image saved to collage")), delay: 0.5)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelAddingFilter(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func setFilters(index: Int) -> CGImage {
        let ciContext = CIContext(options: nil)
        let coreImage = CIImage(image: originalImage.image!)
        let filter = CIFilter(name: "\(CIFilterNames[index])" )
        filter!.setDefaults()
        filter!.setValue(coreImage, forKey: kCIInputImageKey)
        let filteredImageData = filter?.value(forKey: kCIOutputImageKey) as! CIImage
        let filteredImageRef = ciContext.createCGImage(filteredImageData, from: filteredImageData.extent)
        
        return filteredImageRef!
    }
}

