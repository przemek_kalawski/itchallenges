import UIKit
import SCRecorder

class ViewController: UIViewController, FrameExtractorDelegate {
    func captured(image: UIImage) {
        imageView.image = image
    }
    

    var frameExtractor: FrameExtractor!
    
    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        frameExtractor = FrameExtractor()
        frameExtractor.delegate = self
    }
    @IBAction func startRecording(_ sender: Any) {
        frameExtractor.startRecording()
    }
    @IBAction func stopRecording(_ sender: Any) {
        frameExtractor.stopRecording()
    }
    @IBOutlet weak var timeLabel: UILabel!
}
