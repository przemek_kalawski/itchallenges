import Foundation
import UIKit

final class ImagesAPI{
    
    static let shared = ImagesAPI()
    private init(){
        
    }
    
    private let imageManager = ImageManager()
    
    func getImages() -> [CGImage] {
        return imageManager.getImages()
    }
    
    func getImage(index: Int) -> CGImage {
        return imageManager.getImage(index: index)
    }
    
    func addImage(image: CGImage, _ index: Int) {
        imageManager.addImage(image, at: index)
    }
    
    func removeImages() {
        imageManager.removeImages()
    }
    
    func replaceImage(image: CGImage, index: Int) {
        imageManager.replaceImage(image, at: index)
    }
    
    func setCollage(image: UIImage){
        imageManager.setCollage(image: image)
    }
}
