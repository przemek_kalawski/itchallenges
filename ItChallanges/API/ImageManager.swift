import Foundation
import UIKit

final class ImageManager{
    
    private var images = [CGImage]()
    private var collage = UIImage()
    
    func addImage(_ image : CGImage, at index: Int)  {
        if( images.count >= index){
            images.insert(image, at: index)
        }else{
            images.append(image)
        }
    }
    
    func getImages() -> [CGImage] {
        return images
    }
    
    func getImage(index: Int) -> CGImage {
        return images[index]
    }
    
    func removeImages() {
        images = [CGImage]()
    }
    
    func replaceImage(_ image: CGImage, at index: Int){
        images[index] = image
    }
    
    func setCollage(image: UIImage){
        collage = image
    }
}
