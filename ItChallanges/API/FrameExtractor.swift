import AVFoundation
import UIKit
import Vision

public class FrameExtractor : NSObject, AVCaptureVideoDataOutputSampleBufferDelegate {
    
    private let imagesAPI = ImagesAPI.shared
    private var startEnabled = false
    
    weak var delegate: FrameExtractorDelegate?
    
    private let captureSession = AVCaptureSession()
    private let sessionQueue = DispatchQueue(label: "session queue")
    private var permissionGranted = false
    
    private let position = AVCaptureDevice.Position.back
    private let quality = AVCaptureSession.Preset.medium
    
    private let context = CIContext()
    
    private var photos: [(UIImage, String)] = [(UIImage, String)]()
    private var identifiers: [String] = [String]()
    
    override init() {
        super.init()
        
        self.checkPermission()
        sessionQueue.async { [unowned self] in
            self.configureSession()
            self.captureSession.startRunning()
        }
    }
    
    public func startRecording(){
        startEnabled = true
    }
    
    public func stopRecording(){
        startEnabled = false
    }
    
    func checkPermission(){
        switch AVCaptureDevice.authorizationStatus(for: AVMediaType.video) {
        case .authorized:
            permissionGranted = true
            break
        case .notDetermined:
            requestPermission()
            break
        default:
            permissionGranted = false
            break
        }
    }
    
    private func requestPermission(){
        sessionQueue.suspend()
        AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { [unowned self] granted in
            self.permissionGranted = granted
            self.sessionQueue.resume()
        })
    }
    
    private func configureSession(){
        guard permissionGranted else {
            return
        }
        captureSession.sessionPreset = quality
        guard let captureDevice = selectCaptureDevice() else {
            return
        }
        
        guard let captureDeviceInput = try? AVCaptureDeviceInput(device: captureDevice) else {
            return
        }
        
        guard captureSession.canAddInput(captureDeviceInput) else { return }
        captureSession.addInput(captureDeviceInput)
        
        let videoOutput = AVCaptureVideoDataOutput()
        videoOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "sample buffer"))
        
        guard captureSession.canAddOutput(videoOutput) else {
            return
        }
        
        captureSession.addOutput(videoOutput)
        
        guard let connection = videoOutput.connection(with: AVFoundation.AVMediaType.video) else { return }
        guard connection.isVideoOrientationSupported else { return }
        guard connection.isVideoMirroringSupported else { return }
        connection.videoOrientation = .portrait
        connection.isVideoMirrored = position == .front
    }
    
    private func selectCaptureDevice() -> AVCaptureDevice? {
        return AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: position)
    }
    
    public func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        guard let model = try? VNCoreMLModel(for: SqueezeNet().model)  else {
            return
        }
        
        var isGreater: Bool = false
        var identifier: String = ""
        
        let request = VNCoreMLRequest(model: model) { (finishedRequest, error) in
            guard let results = finishedRequest.results as? [VNClassificationObservation] else { return }
            guard let Observation = results.first else { return }
            isGreater = Observation.confidence > 0.65
            identifier = Observation.identifier
        }
        
        guard let pixelBuffer: CVPixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return }
        // executes request
        try? VNImageRequestHandler(cvPixelBuffer: pixelBuffer, options: [:]).perform([request])
        
        guard let uiImage = self.imageFromSampleBuffer(sampleBuffer: sampleBuffer, isGreater: isGreater, identifier: identifier) else { return }
        DispatchQueue.main.async {[unowned self] in
            self.delegate?.captured(image: uiImage)
        }
    }
    
    private func imageFromSampleBuffer(sampleBuffer: CMSampleBuffer, isGreater: Bool, identifier: String ) ->UIImage? {
        guard let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return nil }
        let ciImage = CIImage(cvPixelBuffer: imageBuffer)
        guard let cgImage = context.createCGImage(ciImage, from: ciImage.extent) else {
            return nil
        }
        if startEnabled && imagesAPI.getImages().count < 5 && isGreater && !identifiers.contains(identifier){
            identifiers.append(identifier)
            photos.append((UIImage(cgImage: cgImage), identifier))
            imagesAPI.addImage(image: cgImage, 0)
        }
        return UIImage(cgImage: cgImage)
    }
}


protocol FrameExtractorDelegate: class {
    func captured(image: UIImage)
}
























